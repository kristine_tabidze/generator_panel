import mqtt from 'mqtt';
import connection from '../config/Database.js';

const host = '138.68.77.161'
const port = '1883'
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

const connectUrl = `mqtt://${host}:${port}`

const client = mqtt.connect(connectUrl, {
  clientId,
  clean: true,
  connectTimeout: 4000,
  username: 'levan',
  password: 'gogaladze',
  reconnectPeriod: 1000,
})

const topic = 'info'
client.on('connect', () => {
  console.log('Connected')
  client.subscribe([topic], () => {
    console.log(`Subscribe to topic '${topic}'`)
  })
})


export const sendMessages = async (_req, res) => {
  let msg = {};
  client.on('message', (_topic, payload) => {
    msg = payload.toString();
    console.log(payload.toString(), 'payload.toString()')
  }) 
  try {
    res.json(msg);
  } catch (error) {
    console.log(error);
  }
};

export const sendMessagesFromDb = async (_req, res) => {
  try {
    let records = [];
    connection.query("SELECT * FROM messages", (err, rows, fields) => {
      if (!err) {
        res.json(rows);
      } else {
        res.json(records);
        console.log(err)
      }
    });
  } catch (error) {
    res.json(records);
  }
};