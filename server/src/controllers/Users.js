import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import connection from '../config/Database.js';
 
export const getUsers = async(req, res) => {
    try {
        let users = [];
        connection.query(`SELECT * FROM users where email = ${email}`, (err, rows, fields) => {
          if (!err) {
            users = rows;
          }
        });
    } catch (error) {
        console.log(error);
    }
}
 
export const Register = async(req, res) => {
    const { name, email, password, lastName } = req.body;
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);
    let users = [];
    try {
        connection.query(`INSERT INTO users(name,lastName,email,password) VALUES ('${name}', '${lastName}', '${email}', '${password}')`, (err, rows, fields) => {
          if (!err) {
            users = rows;
          } else {
            console.log(err);
          }
        });
        res.json({msg: "Registration Successful"});
    } catch (error) {
        console.log(error);
    }
}
 
export const Login = async(req, res) => {
  let users = [];
    try {

        // if(!match) return res.status(400).json({msg: "Wrong Password"});
        connection.query(`select * from users where email = '${req.body.email}' and password = '${req.body.password}'`, (err, rows, fields) => {
          if (!err) {
            users = rows;
            res.sendStatus(200);
            
          } else {
            console.log(err, 'err')
          }
        });
        // console.log(users, 'users', req.body.email)
        // const userId = users[0].id;
        // const name = users[0].name;
        // const email = users[0].email;
        // console.log(users, 'users')
        // const accessToken = jwt.sign({userId, name, email}, process.env.ACCESS_TOKEN_SECRET,{
        //     expiresIn: '15s'
        // });
        // const refreshToken = jwt.sign({userId, name, email}, process.env.REFRESH_TOKEN_SECRET,{
        //     expiresIn: '1d'
        // });
        // await Users.update({refresh_token: refreshToken},{
        //     where:{
        //         id: userId
        //     }
        // });
        // res.cookie('refreshToken', refreshToken,{
        //     httpOnly: true,
        //     maxAge: 24 * 60 * 60 * 1000
        // });
        // res.json({ accessToken });
    } catch (error) {
        res.status(404).json({msg:"Email not found"});
    }
}
 
export const Logout = async(req, res) => {
    const refreshToken = req.cookies.refreshToken;
    if(!refreshToken) return res.sendStatus(204);
    const user = await Users.findAll({
        where:{
            refresh_token: refreshToken
        }
    });
    if(!user[0]) return res.sendStatus(204);
    const userId = user[0].id;
    await Users.update({refresh_token: null},{
        where:{
            id: userId
        }
    });
    res.clearCookie('refreshToken');
    return res.sendStatus(200);
}