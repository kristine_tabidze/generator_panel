import mysql from 'mysql';
import dotenv from 'dotenv';
dotenv.config();

// const connection = mysql.createConnection({
// 	host     : process.env.db_host,
// 	user     : process.env.db_user,
// 	password : process.env.db_password,
// 	database : process.env.db_name
// });

const connection = mysql.createConnection({
	host     : process.env.db_host,
	user     : process.env.db_user,
	password : process.env.db_password,
	database : process.env.db_name
});

connection.connect((err)=>{
  if(!err)
  console.log('DB connection succeded.');
  else
  console.log('DB connection failed \n Error : '+ JSON.stringify(err,undefined,2));
});

export default connection;