// var http = require("http");
// const mqtt = require('mqtt')
// const host = '138.68.77.161'
// const port = '1883'
// const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

// const connectUrl = `mqtt://${host}:${port}`

// const client = mqtt.connect(connectUrl, {
//   clientId,
//   clean: true,
//   connectTimeout: 4000,
//   username: 'levan',
//   password: 'gogaladze',
//   reconnectPeriod: 1000,
// })

// const topic = 'info'
// client.on('connect', () => {
//   console.log('Connected')
//   client.subscribe([topic], () => {
//     console.log(`Subscribe to topic '${topic}'`)
//   })
// })


// http.createServer(function (request, response) {
//    // Send the HTTP header 
//    // HTTP Status: 200 : OK
//    // Content Type: text/plain
//    response.writeHead(200, {'Content-Type': 'text/plain'});
   
//    // Send the response body as "Hello World"
//    response.end('Hello World\n');
// }).listen(8081);

// // Console will print the message
// console.log('Server running at http://127.0.0.1:8081/', client.on('message', (topic, payload) => {
//   console.log('Received Message:', topic, payload.toString())
// }));

import express from "express";
import { getUsers, Register, Login, Logout } from "../controllers/Users.js";
import { verifyToken } from "../middleware/VerifyToken.js";
import { refreshToken } from "../controllers/RefreshToken.js";
import {sendMessages, sendMessagesFromDb} from "../controllers/mqtt.js"
 
const router = express.Router();
 
router.get('/messages', sendMessages);
router.get('/messages-db', sendMessagesFromDb)
router.get('/users', verifyToken, getUsers);
router.post('/users', Register);
router.post('/login', Login);
router.get('/token', refreshToken);
router.delete('/logout', Logout);
 
export default router;