import { ISignUpForm } from "../interfaces/Auth";

export const authDefaultCredentials: ISignUpForm = {
    name: '',
    lastname: '',
    email: '',
    password: '',
}