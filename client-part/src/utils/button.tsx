import React from 'react'
import '../styles/auth.scss';

interface IButton {
    text: string;
    onClick: () => void;
}

export const Button: React.FC<IButton> = ({ text, onClick }) => {
    return (
        <button onClick={onClick} className="customButton">
            {text}
        </button>
    )
}