import { Dashboard } from './pages/Dashboard';
import { LandingPage } from './pages/LandingPage';
import { Routes, Route } from 'react-router-dom';
import { Graph } from './pages/Graph';
import { Header } from './components/Header';

function App() {
  return (
    <div className="App">
      <Header />
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route path="/info" element={<Dashboard />} />
          <Route path="/graph" element={<Graph />} />
      </Routes>
    </div>
  );
}

export default App;
