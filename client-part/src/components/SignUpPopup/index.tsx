import { Box, Modal, TextField } from "@mui/material";
import React, { useState } from "react";
import { ISignUpForm } from "../../interfaces/Auth";
import { authDefaultCredentials } from "../../utils/authDefaultCredentials";
import { Button } from "../../utils/button";
import "../../styles/auth.scss";
import axios from "axios";
import { useNavigate } from "react-router-dom";

interface ISignUpPopup {
  open: boolean;
  onClose: () => void;
}

export const SignUpPopup: React.FC<ISignUpPopup> = ({ open, onClose }) => {
  const [credentials, setCredentials] = useState<ISignUpForm>(
    authDefaultCredentials
  );

  const navigate = useNavigate();

  const Register = async () => {
    try {
        await axios.post('http://localhost:5000/users', {
            name: credentials.name,
            email: credentials.email,
            password: credentials.password,
            lastName: credentials.lastname
        });
        onClose();
        navigate("/info");
    } catch (error) {
        console.log(error)
    }
}

  return (
    <Modal open={open} onClose={onClose} aria-labelledby="Sign In Modal">
      <div className="popupContainer">
        <h1>Sign Up</h1>
        <div className="registrationFormWrappersa">
          <div className="fieldsContainers">
            <TextField
              required
              id="name"
              label="Name"
              value={credentials.name}
              onChange={(e) =>
                setCredentials({ ...credentials, name: e.target.value })
              }
              className="input"
            />
            <TextField
              required
              id="lastName"
              label="Last Name"
              value={credentials.lastname}
              onChange={(e) =>
                setCredentials({ ...credentials, lastname: e.target.value })
              }
              className="input"
            />
            <TextField
              required
              id="email"
              label="E-mail"
              value={credentials.email}
              onChange={(e) =>
                setCredentials({ ...credentials, email: e.target.value })
              }
              className="input"
            />
            <TextField
              required
              id="password"
              label="Password"
              value={credentials.password}
              onChange={(e) =>
                setCredentials({ ...credentials, password: e.target.value })
              }
              type={"password"}
              className="input"
            />
          </div>
        </div>
        <Button text="Sign Up" onClick={Register} />
      </div>
    </Modal>
  );
};
