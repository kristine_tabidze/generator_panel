import React, { useMemo } from 'react'
import { IMessage } from '../../interfaces/Message'
import '../../styles/messageCard.scss';

export const MessageCard: React.FC<{message: IMessage}> = ({message}) => {
    const keys = useMemo(() => {
        return Object.keys(message)
    }, [message]);

    const values = useMemo(() => {
        return Object.values(message)
    }, [message]);

    return (
        <div className="messageContainer">
            <div>
                {keys.map((key, i) => (
                    <div key={i} className="messageKey">{key}</div>
                ))}
            </div>
            <div>
                {values.map((value, i) => (
                    <div key={i} className="messageValue">{value}</div>
                ))}
            </div>
        </div>
    )
}