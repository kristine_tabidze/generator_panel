import { TextField, Modal } from "@mui/material";
import React, { useState } from "react";
import { Button } from "../../utils/button";
import "../../styles/auth.scss";
import axios from 'axios';
import { useNavigate } from "react-router-dom";

interface ISignInPopup {
    open: boolean;
    onClose: () => void;
}

export const SignInPopup: React.FC<ISignInPopup> = ({open, onClose}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const navigate = useNavigate();

    const handleSubmit = async () => {
        try {
            await axios.post('http://localhost:5000/login', {
                email: email,
                password: password
            });
            onClose();
            navigate("/info");
        } catch (error) {
            if (error) {
              console.log(error);
            }
        }
    }


  return (
    <Modal
      open={open}
      onClose={handleSubmit}
      aria-labelledby="Sign In Modal"
    >
      <div className="popupContainer">
        <div className="wrapper">
          <h1>Sign In</h1>
          <TextField
              required
              id="email"
              label="E-mail"
              value={email}
              onChange={(e) =>
                setEmail(e.target.value)
              }
              className="input"
            />
            <TextField
              required
              id="password"
              label="Password"
              value={password}
              onChange={(e) =>
                setPassword(e.target.value)
              }
              type={"password"}
              className="input"
            />
          <div>
            <Button text="Sign In" onClick={handleSubmit} />
          </div>
        </div>
      </div>
    </Modal>
  );
};
