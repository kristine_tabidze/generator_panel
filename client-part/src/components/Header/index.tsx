import React, { useCallback, useMemo, useState } from 'react'
import { Button } from '../../utils/button'
import '../../styles/landing.scss';
import { SignInPopup } from '../SignInPopup';
import { SignUpPopup } from '../SignUpPopup';
import { useNavigate } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import axios from 'axios';

export const Header = () => {
    const [isClickedOnSignIn, setIsClickedOnSignIn] = useState(false);
    const [isClickedOnSignUp, setIsClickedOnSignUp] = useState(false);
    const navigate = useNavigate();
    const location = useLocation();
    const isLoggedIn = useMemo(() => {
        return location.pathname.indexOf('info') > -1 || location.pathname.indexOf('graph') > -1;
    }, [location]);

     const logout = async () => {
        try {
            // await axios.delete('http://localhost:5000/logout');
            navigate('/');
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className='headerContainer'>
            <div>
                <div className='logo' onClick={() => navigate('/')} />
            </div>
            <div>
                {isLoggedIn ? (
                    <Button text='Logout' onClick={logout} />
                ) : (
                    <>
                        <Button text={'Sign In'} onClick={() => setIsClickedOnSignIn(true)} >Sign In</Button>
                        <Button text={'Sign Up'} onClick={() => setIsClickedOnSignUp(true)} >Sign In</Button>
                    </>
                )}
            </div>
            {isClickedOnSignIn && <SignInPopup open={isClickedOnSignIn} onClose={() => setIsClickedOnSignIn(false)} />}
            {isClickedOnSignUp && <SignUpPopup open={isClickedOnSignUp} onClose={() => setIsClickedOnSignUp(false)} />}
        </div>
    )
}