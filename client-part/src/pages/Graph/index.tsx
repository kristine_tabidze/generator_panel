import React, { useState, useEffect } from "react";
import "chart.js/auto";
import { Chart } from "react-chartjs-2";
import {
  Chart as ChartJS,
  LineController,
  LineElement,
  PointElement,
  LinearScale,
  Title,
} from "chart.js";
import axios from "axios";

ChartJS.register(LineController, LineElement, PointElement, LinearScale, Title);

const labels = ["1s", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "10s"];

export const Graph = () => {
  useEffect(() => {
    const fetchSamplings = async () => {
      await axios.get("http://localhost:5000/messages-db").then((res) => {
        const data = res.data;
        const dt = [];
        const shuntvoltage = data.map((message: any) =>
          parseFloat(message.shuntvoltage)
        );
        const busvoltage = data.map((message: any) =>
          parseFloat(message.busvoltage)
        );
        const current_mA = data.map((message: any) =>
          parseFloat(message.current_mA)
        );
        const power_mW = data.map((message: any) =>
          parseFloat(message.power_mW)
        );
        const loadvoltage = data.map((message: any) =>
          parseFloat(message.loadvoltage)
        );
        setChartData({
          labels,
          datasets: [
            {
              label: "shuntvoltage",
              data: shuntvoltage,
              borderColor: "rgb(53, 162, 235)",
            },
            {
              label: "busvoltage",
              data: busvoltage,
              borderColor: "#3835eb",
            },
            {
              label: "current_mA",
              data: current_mA,
              borderColor: "#eb359a",
            },
            {
              label: "power_mW",
              data: power_mW,
              borderColor: "#61ed0a",
            },
            {
              label: "loadvoltage",
              data: loadvoltage,
              borderColor: "#edc70a",
            },
          ],
        });
      });
    };
    fetchSamplings();
  }, []);

  const [chartData, setChartData] = useState<any>({
    datasets: [],
  });

  return (
    <div style={{ width: "90vw", margin: "120px auto 0 auto" }}>
      <Chart type="line" data={chartData} />
    </div>
  );
};
