import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { MessageCard } from "../../components/MessageCard/MessageCard";
import { IMessage } from "../../interfaces/Message";
import '../../styles/messageCard.scss';
import { Button } from "../../utils/button";
import { useNavigate } from 'react-router-dom';

export const Dashboard = () => {
  const [info, setInfo] = useState<IMessage[]>([]);
  const navigate = useNavigate();

  const getMessages = useCallback(async () => {
    await axios
      .get("http://localhost:5000/messages")
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }, []);

  const getMessagesFromDb = useCallback(async () => {
    await axios
      .get("http://localhost:5000/messages-db")
      .then((res) => setInfo(res.data))
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    getMessagesFromDb();
  }, []);

  return (
    <div>
      {info.length > 0 ? (
        <div className="dashboardContainer">
            <div className="messageCardsContainer">
            {info.map((message, i) => (
                <MessageCard key={i} message={message} />
            ))}
            </div>
            <Button text="View Graph" onClick={() => navigate('/graph')} />
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};
