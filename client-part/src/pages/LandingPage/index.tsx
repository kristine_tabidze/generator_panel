import React from "react";
import "../../styles/landing.scss";

export const LandingPage: React.FC<{}> = () => {
  return (
    <div className="landingContainer">
      <div className="landingImageProvider">
        <div className="contentContainer">
          <div className="solar-img">
            <div className="info">
              <h1>Welcome To</h1>
              <h1>Solar Panel Measurement System !</h1>
              <div className="mainInfoContainer">
                <p>
                  Solar panels are those devices which are used to absorb the
                  sun's rays and convert them into electricity or heat. Its
                  measurement system is integrative and usable, based on{" "}
                  <span>MQTT</span> server
                </p>
                <p>
                  On this website you can measure the power of your solar. If you are not <a href="/login">don't have an account yet</a> you can sign up <a href="/login">here</a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="dgData">
            <h1>Graphs from Digital Ocean</h1>
          <div className="img-1" />
          <div className="img-2" />
          <div className="img-3" />
          <div className="img-4" />
      </div>
      </div>
    </div>
  );
};
