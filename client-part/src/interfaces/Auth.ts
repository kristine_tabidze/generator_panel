export interface ISignUpForm {
    name: string;
    lastname: string;
    email: string;
    password: string;
}