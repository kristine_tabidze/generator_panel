export interface IMessage {
   shuntvoltage: string
   busvoltage: string
   current_mA: string
   power_mW: string
   loadvoltage: string
}